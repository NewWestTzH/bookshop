<?php
	/**
	* 
	*/
	class Login_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function admin_login($email,$password)
		{
			$this->db->select('id,email,password');
			$this->db->from('admin');
			$this->db->where('email',$email);
			$this->db->where('password',$password);
			$this->db->limit(1);
			$query=$this->db->get();
			if($query->num_rows()==1)
			{
				return $query->row();
			}
			else
			{
				return false;
			}
		}
	}
?>