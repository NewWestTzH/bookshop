<?php
	/**
	* 
	*/
	class Genre_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function select_all()
		{
			$this->db->select('*');
			$this->db->from('genre');
			$query=$this->db->get();
			// var_dump($query);die();
			return $query->result();
		}
		function insert($genre)
		{
			$this->db->insert('genre', $genre); 
		}
		function update($genre,$genre_id)
		{
			$this->db->where('id',$genre_id);
			$this->db->update('genre',$genre);
		}
	}
?>