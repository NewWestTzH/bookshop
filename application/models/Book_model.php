<?php
	/**
	* 
	*/
	class Book_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function select_all_book()
		{
			$this->db->select('*,book.id as book_id');
			$this->db->from('book');
			$this->db->join('author','author.id=book.author_id');
			$this->db->join('genre','genre.id=book.genre_id');
			$this->db->join('publisher','publisher.id=book.publishing_house_id');
			$this->db->where('deleted',0);
			$query=$this->db->get();
			return $query->result();
		}
		function insert($book)
		{
			$this->db->set($book);
			$this->db->insert('book');
		}
		function select_by_code($code_number)
		{
			$this->db->select('*');
			$this->db->from('book');
			$this->db->where('code_number',$code_number);
			$query=$this->db->get();
			return $query->result();
		}
		function check_for_code($code_number,$book_id)
		{
			$this->db->select('id');
			$this->db->from('book');
			$this->db->where('code_number',$code_number);
			$this->db->where('id!=',$book_id);
			$query=$this->db->get();
			return $query->result();
		}
		function select_by_id($id)
		{
			$this->db->select('*,book.id as book_id');
			$this->db->from('book');
			$this->db->join('author','author.id=book.author_id');
			$this->db->join('genre','genre.id=book.genre_id');
			$this->db->join('publisher','publisher.id=book.publishing_house_id');
			$this->db->where('deleted',0);
			$this->db->where('book.id',$id);
			$query=$this->db->get();
			return $query->row();
		}
		function delete($book,$book_id)
		{
			$this->db->where('id',$book_id);
			$this->db->update('book',$book);
		}
		function update($book,$book_id)
		{
			$this->db->where('id',$book_id);
			$this->db->update('book',$book);
		}
		function select_by_author($id)
		{
			$this->db->select('*,book.id as book_id');
			$this->db->from('book');
			$this->db->join('author','author.id=book.author_id');
			$this->db->join('genre','genre.id=book.genre_id');
			$this->db->join('publisher','publisher.id=book.publishing_house_id');
			$this->db->where('deleted',0);
			$this->db->where('author.id',$id);
			$query=$this->db->get();
			return $query->result();
		}
		function select_by_genre($id)
		{
			$this->db->select('*,book.id as book_id');
			$this->db->from('book');
			$this->db->join('author','author.id=book.author_id');
			$this->db->join('genre','genre.id=book.genre_id');
			$this->db->join('publisher','publisher.id=book.publishing_house_id');
			$this->db->where('deleted',0);
			$this->db->where('genre.id',$id);
			$query=$this->db->get();
			return $query->result();
		}
		function select_by_publisher($id)
		{
			$this->db->select('*,book.id as book_id');
			$this->db->from('book');
			$this->db->join('author','author.id=book.author_id');
			$this->db->join('genre','genre.id=book.genre_id');
			$this->db->join('publisher','publisher.id=book.publishing_house_id');
			$this->db->where('deleted',0);
			$this->db->where('publisher.id',$id);
			$query=$this->db->get();
			return $query->result();
		}
		function customer_select()
		{
			$this->db->select('*');
			$this->db->from('book');
			$this->db->where('deleted',0);
			$query=$this->db->get();
			return $query->result();
		}
		function customer_browse_by_author($author_id)
		{
			$this->db->select('*');
			$this->db->from('book');
			$this->db->where('author_id',$author_id);
			$this->db->where('deleted',0);
			$query=$this->db->get();
			return $query->result();
		}
		function customer_browse_by_genre($genre_id)
		{
			$this->db->select('*');
			$this->db->from('book');
			$this->db->where('genre_id',$genre_id);
			$this->db->where('deleted',0);
			$query=$this->db->get();
			return $query->result();
		}
		function customer_browse_by_publisher($publisher_id)
		{
			$this->db->select('*');
			$this->db->from('book');
			$this->db->where('publishing_house_id',$publisher_id);
			$this->db->where('deleted',0);
			$query=$this->db->get();
			return $query->result();
		}
		function select_new_book()
		{
			$this->db->select("*");
			$this->db->from("book");
			$this->db->where("deleted",0);
			$this->db->order_by("id", "desc");
			$this->db->limit(8); 
			$query=$this->db->get();
			return $query->result();
		}
	}
?>