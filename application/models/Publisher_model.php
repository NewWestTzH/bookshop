<?php
	/**
	* 
	*/
	class Publisher_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function select_all()
		{
			$this->db->select('*');
			$this->db->from('publisher');
			$query=$this->db->get();
			// var_dump($query);die();
			return $query->result();
		}
		function insert($publisher)
		{
			$this->db->insert('publisher', $publisher); 
		}
		function update($publisher,$publisher_id)
		{
			$this->db->where('id',$publisher_id);
			$this->db->update('publisher',$publisher);
		}
	}
?>