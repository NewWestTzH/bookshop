<?php
	/**
	* 
	*/
	class Author_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function select_all()
		{
			$this->db->select('*');
			$this->db->from('author');
			$query=$this->db->get();
			// var_dump($query);die();
			return $query->result();
		}
		function insert($author)
		{
			$this->db->insert('author', $author); 
		}
		function update($author,$author_id)
		{
			$this->db->where('id',$author_id);
			$this->db->update('author',$author);
		}
	}
?>