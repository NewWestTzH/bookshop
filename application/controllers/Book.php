<?php
	/**
	* 
	*/
	class Book extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model("book_model");
			$this->load->model("author_model");
			$this->load->model("genre_model");
			$this->load->model("publisher_model");
			$this->load->library("form_validation");
		}
		function index()
		{
			if($this->session->userdata('id'))
			{
				$this->admin_browse();
			}
			else
			{
				$this->customer_browse();
			}
		}
		function customer_browse()
		{
			$data['book']=$this->book_model->select_new_book();
			$this->load->view("includes/header");
			$this->load->view("includes/nav_bar");
			$this->load->view("customer/home_view",$data);
		}
		function customer_all_book()
		{
			if($this->session->userdata('id'))
			{
				$this->admin_browse();
			}
			else
			{
				$this->load->library('pagination');

				$config['base_url'] = 'http://localhost/book_store/';
				$config['total_rows'] = 200;
				$config['per_page'] = 20;

				$this->pagination->initialize($config);

				$data['book']=$this->book_model->customer_select();
				$data['author']=$this->author_model->select_all();
				$data['genre']=$this->genre_model->select_all();
				$data['publisher']=$this->publisher_model->select_all();
				$this->load->view("includes/header");
				$this->load->view("includes/nav_bar");
				$this->load->view('customer/all_view',$data);
			}
		}
		function customer_search_by_author()
		{
			if($this->session->userdata('id'))
			{
				$this->admin_browse();
			}
			else
			{
				$data['book']=$this->book_model->customer_select();
				$data['author']=$this->author_model->select_all();
				$this->load->view("includes/header");
				$this->load->view("includes/nav_bar");
				$this->load->view('customer/author_view',$data);
			}
		}
		function customer_search_by_genre()
		{
			if($this->session->userdata('id'))
			{
				$this->admin_browse();
			}
			else
			{
				$data['book']=$this->book_model->customer_select();
				$data['genre']=$this->genre_model->select_all();
				$this->load->view("includes/header");
				$this->load->view("includes/nav_bar");
				$this->load->view('customer/genre_view',$data);
			}
		}
		function customer_search_by_publisher()
		{
			if($this->session->userdata('id'))
			{
				$this->admin_browse();
			}
			else
			{
				$data['book']=$this->book_model->customer_select();
				$data['publisher']=$this->publisher_model->select_all();
				$this->load->view("includes/header");
				$this->load->view("includes/nav_bar");
				$this->load->view('customer/publisher_view',$data);
			}
		}
		function customer_detail($book_id)
		{
			if($this->session->userdata('id'))
			{
				$this->admin_browse();
			}
			else
			{
				$data['book']=$this->book_model->select_by_id($book_id);
				$this->load->view("includes/header");
				$this->load->view("includes/nav_bar");
				$this->load->view('customer/detail_view',$data);
			}
		}
		function customer_browse_book_by_category()
		{
			if($this->session->userdata('id'))
			{
				$this->admin_browse();
			}
			else
			{
				$type=$this->input->post('type');
				$id=$this->input->post('id');
				if($type=="author")
				{
					$data['book']=$this->book_model->customer_browse_by_author($id);
				}
				elseif($type=="genre")
				{
					$data['book']=$this->book_model->customer_browse_by_genre($id);
				}
				else
				{
					$data['book']=$this->book_model->customer_browse_by_publisher($id);
				}
				$this->load->view('customer/book_image_view',$data);
			}
		}
		function admin_browse()
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$data['book']=$this->book_model->select_all_book();
			$this->load->view("includes/header");
			$this->load->view("includes/nav_bar");
			$this->load->view('admin/browse_view',$data);
			$this->load->view("includes/footer");
		}
		function book_detail($book_id)
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$data['book']=$this->book_model->select_by_id($book_id);
			$this->load->view("includes/header");
			$this->load->view("includes/nav_bar");
			$this->load->view('admin/detail_view',$data);
		}
		function delete_book($book_id)
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$book['deleted']=1;
			$this->book_model->delete($book,$book_id);
			$this->admin_browse();
		}
		function update_book($book_id)
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$data['book']=$this->book_model->select_by_id($book_id);
			$data['author']=$this->author_model->select_all();
			$data['genre']=$this->genre_model->select_all();
			$data['publisher']=$this->publisher_model->select_all();
			$this->load->view("includes/header");
			$this->load->view("includes/nav_bar");
			$this->load->view("admin/update_book_view",$data);
		}
		function update_db()
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$this->form_validation->set_rules( 'code_number' , 'Code Number' ,'trim|required');
			$this->form_validation->set_rules( 'book_name' , 'Book Name' ,'trim|required' );
			$this->form_validation->set_rules( 'price' , 'Price' ,'trim|required' );
			$this->form_validation->set_rules( 'edition' , 'Edition Date' ,'trim|required' );
			$this->form_validation->set_rules( 'author' , 'Author Name' ,'trim|required' );
			$this->form_validation->set_rules( 'genre' , 'Genre Name' ,'trim|required' );
			$this->form_validation->set_rules( 'publisher' ,'Publisher Name' ,'trim|required');
			$code_duplicate=$this->check_same_code($this->input->post('code_number'),$this->input->post('book_id'));
			// var_dump($code_duplicate);die();
			if($code_duplicate==true)
			{
				$this->form_validation->set_message('Code number is already exist. Please input another number.');
				$this->admin_browse();
			}
			else
			{
				if($this->form_validation->run()==false)
				{
					$this->admin_browse();
				}
				else
				{
					$book['code_number']=$this->input->post('code_number');
					$book['name']=$this->input->post('book_name');
					$book['author_id']=$this->input->post('author');
					$book['genre_id']=$this->input->post('genre');
					$book['publishing_house_id']=$this->input->post('publisher');
					$book['price']=$this->input->post('price');
					$book['edition']=$this->input->post('edition');
					if(!empty($_FILES['pic']['name']))
					{
						$config['upload_path'] = './image/';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size'] = '16000';
						$config['max_width']  = '8000';
						$config['max_height']  = '8000';
						$config['remove_spaces'] = TRUE;

						$this->load->library('upload', $config);
						if(!$this->upload->do_upload('pic'))
						{
							$this->admin_browse();
						}
						else
						{
							$data = $this->upload->data();
							$img=$data['file_name'];
							$book['image']=$img;
						}
					}
					else
					{
						$book['image']=$this->input->post('pic_name');
					}
					if(!empty($_FILES['pdf']['name']))
							{
								$config['upload_path'] = './pdf/';
								$config['allowed_types'] = 'pdf|docx';
								$config['max_size'] = '16000';
								$config['remove_spaces'] = TRUE;

								$this->upload->initialize($config);
								$this->load->library('upload', $config);
								if(!$this->upload->do_upload('pdf'))
								{
									$this->admin_browse();
								}
								else
								{
									$data = $this->upload->data();
									$pdf=$data['file_name'];
									$book['save_pdf']=$pdf;
									
								}
							}
					else
					{
						$book['save_pdf']=$this->input->post('pdf_name');
					}
					$book_id=$this->input->post('book_id');				
					$this->book_model->update($book,$book_id);
					$this->admin_browse();
				}
			}
		}
		function admin_browse_category()
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$data['book']=$this->book_model->select_all_book();
			$data['author']=$this->author_model->select_all();
			$data['genre']=$this->genre_model->select_all();
			$data['publisher']=$this->publisher_model->select_all();
			$this->load->view("includes/header");
			$this->load->view("includes/nav_bar");
			$this->load->view('admin/category_list_view',$data);
			$this->load->view("includes/footer");
		}
		function browse_book_by_category()
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$type=$this->input->post('type');
			$id=$this->input->post('id');
			// var_dump($type);
			// var_dump($id);
			if($type=="author")
			{
				$data['book']=$this->book_model->select_by_author($id);
			}
			elseif($type=="genre")
			{
				$data['book']=$this->book_model->select_by_genre($id);
			}
			else
			{
				$data['book']=$this->book_model->select_by_publisher($id);
			}
			// var_dump($data);die();
			$this->load->view('admin/book_table_view',$data);
		}
		function create_book()
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$this->load->view("includes/header");
			$this->load->view("includes/nav_bar");
			$data['author']=$this->author_model->select_all();
			$data['genre']=$this->genre_model->select_all();
			$data['publisher']=$this->publisher_model->select_all();
			$this->load->view("admin/create_new_view",$data);
		}
		function insert_book()
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$this->form_validation->set_rules( 'code_number' , 'Code Number' ,'trim|required|callback_check_code' );
			$this->form_validation->set_rules( 'book_name' , 'Book Name' ,'trim|required' );
			$this->form_validation->set_rules( 'price' , 'Price' ,'trim|required' );
			$this->form_validation->set_rules( 'edition' , 'Edition Date' ,'trim|required' );
			$this->form_validation->set_rules( 'author' , 'Author Name' ,'trim|required' );
			$this->form_validation->set_rules( 'genre' , 'Genre Name' ,'trim|required' );
			$this->form_validation->set_rules( 'publisher' , 'Publisher Name' ,'trim|required' );
			// $this->form_validation->set_rules( 'pic' , 'Image' ,'trim|required' );
			// $this->form_validation->set_rules( 'pdf' , 'Pdf File' ,'trim|required' );
			if($this->form_validation->run()==false)
			{
				$this->create_book();
			}
			else
			{
				$book['code_number']=$this->input->post('code_number');
				$book['name']=$this->input->post('book_name');
				$book['author_id']=$this->input->post('author');
				$book['genre_id']=$this->input->post('genre');
				$book['publishing_house_id']=$this->input->post('publisher');
				$book['price']=$this->input->post('price');
				$book['edition']=$this->input->post('edition');
				if(!empty($_FILES['pic']['name']))
				{
					$config['upload_path'] = './image/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size'] = '16000';
					$config['max_width']  = '8000';
					$config['max_height']  = '8000';
					$config['remove_spaces'] = TRUE;

					$this->load->library('upload', $config);
					if(!$this->upload->do_upload('pic'))
					{
						// $error = array('error' => $this->upload->display_errors());
						// $this->load->view("includes/header");
						// $this->load->view("includes/nav_bar");
						// $data['author']=$this->author_model->select_all();
						// $data['genre']=$this->genre_model->select_all();
						// $data['publisher']=$this->publisher_model->select_all();
						// $this->load->view("admin/create_new_view",$data,$error);
						$this->create_book();
					}
					else
					{
						$data = $this->upload->data();
						$img=$data['file_name'];
						$book['image']=$img;
						if(!empty($_FILES['pdf']['name']))
						{
							$config['upload_path'] = './pdf/';
							$config['allowed_types'] = 'pdf|docx';
							$config['max_size'] = '16000';
							$config['remove_spaces'] = TRUE;

							$this->upload->initialize($config);
							$this->load->library('upload', $config);
							if(!$this->upload->do_upload('pdf'))
							{
								// $error = array('error' => $this->upload->display_errors());
								// $this->load->view("includes/header");
								// $this->load->view("includes/nav_bar");
								// $data['author']=$this->author_model->select_all();
								// $data['genre']=$this->genre_model->select_all();
								// $data['publisher']=$this->publisher_model->select_all();
								// $this->load->view("admin/create_new_view",$data,$error);
								$this->create_book();
							}
							else
							{
								$data = $this->upload->data();
								$pdf=$data['file_name'];
								$book['save_pdf']=$pdf;
								$this->book_model->insert($book);
								$this->admin_browse();
							}
						}
						else
						{
							$this->create_book();
						}
					}
				}
				else
				{
					$this->create_book();
				}
			}
		}
		function check_code($code_number)
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$data=$this->book_model->select_by_code($code_number);
			if($data)
			{
				$this->form_validation->set_message('check_code','Code number is already exist. Please input another number.');
				return false;
			}
			else
			{
				return true;
			}
		}
		function check_same_code($code_number,$book_id)
		{
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
			$data=$this->book_model->check_for_code($code_number,$book_id);
			// var_dump($data);die();
			if($data)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
?>