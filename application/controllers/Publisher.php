<?php
	/**
	* 
	*/
	class Publisher extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model("publisher_model");
			$this->load->library("form_validation");
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
		}
		function index()
		{
			$this->browse_publisher();
		}
		function browse_publisher()
		{
			$data['query']=$this->publisher_model->select_all();
			$this->load->view("includes/header");
			$this->load->view("includes/nav_bar");
			$this->load->view("admin/publisher_view",$data);
			$this->load->view("includes/footer");
		}
		function save_publisher()
		{

			$this->form_validation->set_rules('publisher_name','Publisher name','trim|required');
			if($this->form_validation->run()==false)
			{
				$this->browse_publisher();
			}
			else
			{
				$publisher['publisher_name']=$this->input->post('publisher_name');
				$this->publisher_model->insert($publisher);
				$this->browse_publisher();
			}
		}
		function modify_publisher()
		{
			$publisher['publisher_name']=$this->input->post("publisher_name");
			$publisher['publisher_id']=$this->input->post("publisher_id");
			$this->load->view('admin/update_publisher_view',$publisher);
		}
		function update_publisher()
		{
			$publisher['publisher_name']=$this->input->post("publisher_name");
			$publisher_id=$this->input->post("publisher_id");
			$this->publisher_model->update($publisher,$publisher_id);
			$this->browse_publisher();
		}
	}
?>