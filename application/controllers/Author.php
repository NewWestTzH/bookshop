<?php
	/**
	* 
	*/
	class Author extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model("author_model");
			$this->load->library("form_validation");
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
		}
		function index()
		{
			$this->browse_author();
		}
		function browse_author()
		{
			$data['query']=$this->author_model->select_all();
			$this->load->view("includes/header");
			$this->load->view("includes/nav_bar");
			$this->load->view("admin/author_view",$data);
			$this->load->view("includes/footer");
		}
		function save_author()
		{
			$this->form_validation->set_rules('author_name','Author name','trim|required');
			if($this->form_validation->run()==false)
			{
				$this->browse_author();
			}
			else
			{
				$author['author_name']=$this->input->post('author_name');
				$this->author_model->insert($author);
				$this->browse_author();
			}
		}
		function modify_author()
		{
			
			$author['author_name']=$this->input->post('author_name');
			$author['author_id']=$this->input->post('author_id');
			$this->load->view('admin/update_author_view',$author);
		}
		function update_author()
		{
			$author['author_name']=$this->input->post('author_name');
			$author_id=$this->input->post('author_id');
			$this->author_model->update($author,$author_id);
			$this->browse_author();
		}
	}
?>