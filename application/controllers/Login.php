<?php
	/**
	* 
	*/
	class Login extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function index()
		{
			if($this->session->userdata('id')==false)
			{
				$this->load->view("includes/header");
				$this->load->view("includes/nav_bar");
				$this->load->view("admin/login_view");
			}
			else
			{
				redirect('book','refresh');
			}
		}
		function verify_login()
		{
			if($this->session->userdata('id')==false)
			{
				$this->load->helper('security');
				$this->load->library( 'form_validation' );
				$this->form_validation->set_rules( 'email' , 'Email' ,
				'trim|required|xss_clean|valid_email' );
				$this->form_validation->set_rules( 'password' , 'Password' ,
				'trim|required|xss_clean|callback_check_database' );

				if($this->form_validation->run() == FALSE)
				{
					$this->index();
				}
				else
				{			
					redirect('book' , 'refresh' );
				}
			}
			else
			{
				redirect('book' , 'refresh' );
			}
		}
		function check_database($password)
		{
			if($this->session->userdata('id')==false)
			{
				$email=$this->input->post('email');
				$this->load->model('login_model');
				$result=$this->login_model->admin_login($email,$password);
				if($result)
				{
					$this->session->set_userdata('id',$result->id);
					return true;
				}
				else
				{
					$this->form_validation->set_message('check_database','Invalid email or password');
					return false;
				}
			}
			else
			{
				redirect('book' , 'refresh' );
			}
		}
		function logout()
		{
			if($this->session->userdata('id'))
			{
				$this->session->unset_userdata('id');
				$this->session->sess_destroy();
			}
				$this->index();
		}
	}
?>