if($this->form_validation->run()==false)
			{
				$this->create_book();
			}
			else
			{
				$book['code_number']=$this->input->post('code_number');
				$book['name']=$this->input->post('book_name');
				$book['author_id']=$this->input->post('author');
				$book['genre_id']=$this->input->post('genre');
				$book['publishing_house_id']=$this->input->post('publisher');
				$book['price']=$this->input->post('price');
				$book['edition']=$this->input->post('edition');
				$uploader="imageupload";

					if (!empty($_FILES['pic']['name']))
					{
						$config['upload_path'] = './image/';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size'] = '16000';
						$config['max_width']  = '8000';
						$config['max_height']  = '8000';
						$config['remove_spaces'] = TRUE;

						$this->load->library('upload', $config);
						if(!$this->upload->do_upload('pic'))
						{
							$error = array('error' => $this->upload->display_errors());
							$this->load->view("includes/header");
							$this->load->view("includes/nav_bar");
							$data['author']=$this->author_model->select_all();
							$data['genre']=$this->genre_model->select_all();
							$data['publisher']=$this->publisher_model->select_all();
							$this->load->view("admin/create_new_view",$data,$error);
						}
						else
						{
							$data = $this->upload->data();
							$img=$data['file_name'];
							if (!empty($_FILES['pdf']['name']))
							{
								$config['upload_path'] = './pdf/';
								$config['allowed_types'] = 'pdf|docx';
								$config['max_size'] = '16000';
								$config['remove_spaces'] = TRUE;

								$this->upload->initialize($config);
								$this->load->library('upload', $config);
								if(!$this->upload->do_upload('pdf'))
								{
									$error = array('error' => $this->upload->display_errors());
									$this->load->view("includes/header");
									$this->load->view("includes/nav_bar");
									$data['author']=$this->author_model->select_all();
									$data['genre']=$this->genre_model->select_all();
									$data['publisher']=$this->publisher_model->select_all();
									$this->load->view("admin/create_new_view",$data,$error);
								}
								else
								{
									$data = $this->upload->data();
								 	$pdf=$data['file_name'];
								}
							}
							else
							{
								
							}
						}
					}
					else
					{
						$this->create_book();
					}
				$this->book_model->insert($book);
			}