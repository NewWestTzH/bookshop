<?php
	/**
	* 
	*/
	class Genre extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model("genre_model");
			$this->load->library("form_validation");
			if(!$this->session->userdata('id'))
			{
				redirect('login','refresh');
			}
		}
		function index()
		{
			$this->browse_genre();
		}
		function browse_genre()
		{
			$data['query']=$this->genre_model->select_all();
			$this->load->view("includes/header");
			$this->load->view("includes/nav_bar");
			$this->load->view("admin/genre_view",$data);
			$this->load->view("includes/footer");
		}
		function save_genre()
		{
			$this->form_validation->set_rules('genre_name','Genre name','trim|required');
			if($this->form_validation->run()==false)
			{
				$this->browse_genre();
			}
			else
			{
				$genre['genre_name']=$this->input->post('genre_name');
				$this->genre_model->insert($genre);
				$this->browse_genre();
			}
		}
		function modify_genre()
		{
			$genre['genre_name']=$this->input->post("genre_name");
			$genre['genre_id']=$this->input->post("genre_id");
			$this->load->view('admin/update_genre_view',$genre);
		}
		function update_genre()
		{
			$genre['genre_name']=$this->input->post("genre_name");
			$genre_id=$this->input->post("genre_id");
			$this->genre_model->update($genre,$genre_id);
			$this->browse_genre();
		}
	}
?>