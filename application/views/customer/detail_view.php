<title>Detail</title>
	   <style type="text/css">
	   	body
		{
			background-color:#ffdccc;
		}
	   	tr
		{
			background-color: #00e6ac;
		}
		.body
		{
			margin: 0 auto;
		}
		.description
		{
			background-color: #e6f2ff;;
			height: 150px;
			width: 100%;
			margin: 0px 0px 15px 0px;
		}
		.text
		{
			padding: 25px 0px 0px 25px;
		}
		.image
		{
			margin: 0px 0px 0px 100px;
		}
		.btn
		{
			float: right;
		}
		.icon
		{
			float: right;
			margin-right: 250px;
		}
		em
		{
			font-size: 20;
			color: #800000;
			margin: 0px 0px 0px 10px;
		}
		.back
		{
			margin-bottom: 20px;
		}
	   </style>
	</head>
	<body>
		<div class="row container body">
			<div class="col-md-6">
				<div class="image">
					<img src="<?php echo base_url(); ?>image/<?php echo $book->image ?>" class="thumbnail">
					<em>Code No:<?php echo " ".$book->code_number;?></em>
					<div class="icon">
					<a href="<?php echo base_url(); ?>pdf/<?php echo $book->save_pdf;?>"><span class="glyphicon glyphicon-arrow-down"></span></a>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<table class="display dt-responsive table-striped table-bordered table">
						<tr>
							<th>Name</th>
							<th><?php echo $book->name; ?></th>
						</tr>
						<tr>
							<th>Author</th>
							<th><?php echo $book->author_name; ?></th>
						</tr>
						<tr>
							<th>Genre</th>
							<th><?php echo $book->genre_name; ?></th>
						</tr>
						<tr>
							<th>Publisher</th>
							<th><?php echo $book->publisher_name; ?></th>
						</tr>
						<tr>
							<th>Price</th>
							<th><?php echo $book->price; ?></th>
						</tr>
						<tr>
							<th>Edition</th>
							<th><?php echo $book->edition; ?></th>
						</tr>
				</table>
			</div>
		</div>
		<div class="col-md-12">
			<h2>Description</h2>
			<div class="description">
				<div class="text">..<?php echo $book->description; ?></div>
			</div>
		</div>
		<div class="row col-md-12">
			<div class="col-md-2 back"><a href="<?php echo base_url();?>book"><em><span class=" glyphicon glyphicon-chevron-left"></span><span class=" glyphicon glyphicon-chevron-left"></span>Back</em></a></div>
		</div>
	</body>
</html>