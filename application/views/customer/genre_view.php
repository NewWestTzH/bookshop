<title>Genre</title>
		<style type="text/css">
		body
			{
			background-color: #ffdccc;
			}
		hr
		{
			color: #00cca3;
			background-color: #00cca3;
			height: 5px;
			width: 100%;
		}
		em
		{
			font-size: 20;
			color: #800000;
			margin: 0px 0px 0px 18px;
		}
		.header
		{
			font-size: 35;
			font-style: oblique;
			color: #800000;
		}
		.heading
		{
			padding: 0px 0px 30px 30px;
		}
		.container
		{
			margin: 90px 0px 0px 20px;
		}
		.category
		{
			padding: 10px 0px 0px 20px;
			color: white;
		}
		.col-md-4
		{
			padding-bottom: 5px;
		}
		.code
		{
			padding: 10px 0px 0px 0px;
		}
		.box
		{
			margin: 0px 0px 0px 30px;
		}
		.image
		{
			height: 230px;
			overflow: hidden;!important
		}
		.img_box
		{
			padding-bottom: 30px;
		}
		.btn
		{
			float: right;
			margin-right: 20px;
		}
		.list_head
		{
			background-color: #ff6666;
		}
		.list_body
		{
			background-color: #00cca3;
			color: white;
		}
		.page_navigation
		{
			padding-bottom: 10px;
			margin: 0px 0px 200px 300px;
		}

		.page_navigation a
		{
			padding:3px 5px;
			margin:2px;
			color:white;
			text-decoration:none;
			float: left;
			font-family: Tahoma;
			font-size: 12px;
			background-color:#00cca3;
		}
		.banner
		{
			background-color: #ff6666;
			margin-left: 20px;
			border-radius: 8px;
		}
		.ban
		{
			margin-top: 30px;
		}
		.book_images
		{
			margin-left: -30px;
		}
		</style>
	<body>
		<!-- heading -->
		<div class="row col-md-12">
			<div class="col-md-5"><hr></div>
			<div class="col-md-3 heading"><p class="header">Genre View<p></div>
			<div class="col-md-4"><hr></div>
		</div>
		<!-- heading -->

		<!-- <div class="container"> -->
			<div class="row box">
				<div class="col-md-2">
					<li class="list-group-item list_head"><a href="#genre" data-toggle="collapse">
						<h4 class="category">Genre</h4>
					</a></li>
					<div id="genre" class="collapse">
						<?php 
							foreach($genre as $genre_list) 
							{
								?><li class="list-group-item list_body"><p OnClick="get_genre_data('genre','<?php echo $genre_list->id;?>')" style="cursor: pointer;"><?php echo $genre_list->genre_name ?></p></li>
								<?php
							}
						?> 
					</div>
				</div>
				<div class="book_images col-md-7">	<!-- book_images -->
					<div id="wrapper">    <!-- wrapper -->
						<div class="row" id="paging_container7">  <!-- row -->
							<!-- <div id="paging_container1" class="container">  paging_container1 --> 
								<ul class="content">
								<?php
									foreach($book as $book_list) 
									{
									?>
									<div class="col-md-4 img_box">
										<div class="image">
											<li><img src="<?php echo base_url(); ?>image/<?php echo $book_list->image;?>" class="thumbnail"></li>
										</div>
										<div class="row code">
											<em>Code No:<?php echo " ".$book_list->code_number;?></em>
											<a href="<?php echo base_url();?>book/customer_detail/<?php echo $book_list->id; ?>"><button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button></a>
										</div>
									</div>
									<?php
									}
									?>
									</ul>
							<!-- </div>		//paging_container1 -->
							<div class="page_navigation col-md-10"></div>
						</div>	<!-- //row -->
					</div>		<!-- //wrapper -->
				</div> <!-- //book_images -->
				<div class="banner col-md-3">
					<!-- <div class="col-md-1"> -->
						<img src="<?php echo base_url(); ?>image/bookstore2.png" class="thumbnail ban">
				<!-- 	</div>
					<div class="col-md-1"> -->
						<img src="<?php echo base_url(); ?>image/bookstore1.jpg" class="thumbnail ban">
					<!-- </div><div class="col-md-1"> -->
						<img src="<?php echo base_url(); ?>image/bookstore3.jpg" class="thumbnail ban">
				</div>
			<!-- </div> -->
			</div>
		<script src="<?php echo base_url();?>public/js/jquery.pajinate.js"></script>
		<script type="text/javascript">
    	$(document).ready(function(){
				$('#paging_container7').pajinate({
					num_page_links_to_display : 3,
					items_per_page : 6,
					nav_label_first : '<<',
					nav_label_last : '>>',
					nav_label_prev : '<',
					nav_label_next : '>'		
				});
			});
  		</script>
		 <script type="text/javascript">
			function get_genre_data(type,id)
			{
				jQuery.ajax(
					{
						url:  "<?php echo base_url();?>book/customer_browse_book_by_category",
						data:{'id':id ,'type':type},
						type: "POST",
						success:function(data)
							{
								$(".book_images").html(data);
							},
						error:function (error){alert('error');}
					}
							);
			}
		</script>
	</body>
</html>