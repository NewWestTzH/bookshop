<title>Home</title>
		<style type="text/css">
		body
		{
			background-color:#ffdccc;
		}
		.header
		{
			font-size: 35;
			font-style: oblique;
			color: #800000;
		}
			
		.heading
		{
			padding: 30px 0px 30px 0px;
		}
		.images
		{
			margin: 0px 0px 0px 50px
		}
		.btn
		{
			float: right;
			margin-right: 80px;
		}
		hr
		{
			color: #00cca3;
			background-color: #00cca3;
			height: 5px;
			width: 100%;
		}
		em
		{
			font-size: 20;
			color: #800000;
			margin: 0px 0px 0px 18px;
		}
		.image
		{
			height: 250px;
			overflow: hidden;!important
		}
		.pic
		{
			height:250px;
			overflow: hidden;!important
		}
		.img_box
		{
			padding-bottom: 30px;
		}
		.slide
		{
			background-color: white;
			height: 300px;
			margin: -18px 0px 0px 0px;
			padding: 18px 0px 0px 0px;
		}
		.code
		{
			padding: 10px 0px 0px 0px;
		}
	</style>
	<body>
		<div class="slide">
			<br>
	  		<div id="myCarousel" class="carousel slide" data-ride="carousel">
	    		<!-- Indicators -->
	    		<ol class="carousel-indicators">
			      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			      <li data-target="#myCarousel" data-slide-to="1"></li>
			      <li data-target="#myCarousel" data-slide-to="2"></li>
			      <li data-target="#myCarousel" data-slide-to="3"></li>
	    		</ol>

			    <!-- Wrapper for slides -->
			    <div class="carousel-inner" role="listbox">
			    	<div class="item active" align="center">
			    		<div class="pic">
			        		<img src="<?php echo base_url(); ?>image/slider1.jpg" width="1000px" height="250px">
			        	</div>
			      	</div>

				    <div class="item" align="center">
				    	<div class="pic">
				     	<img src="<?php echo base_url(); ?>image/slider2.jpg" width="1000px" height="250px">
				     	</div>
				    </div>
			    
			     	<div class="item" align="center">
			     		<div class="pic">
			        	<img src="<?php echo base_url(); ?>image/slider3.jpg" width="1000px" height="250px">
			        	</div>
			     	</div>

			     	<div class="item" align="center">
			     		<div class="pic">
			        	<img src="<?php echo base_url(); ?>image/slider4.jpg" width="1000px" height="250px">
			        	</div>
			     	</div>
			    </div>

			    <!-- Left and right controls -->
			    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			     	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			     	<span class="sr-only">Previous</span>
			    </a>
			    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			     	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    	<span class="sr-only">Next</span>
			    </a>
			</div>
			<div class="row col-md-12 heading">
				<div class="col-md-5"><hr></div>
				<div class="col-md-2"><p class="header">New Arrival</p></div>
				<div class="col-md-5"><hr></div>
			</div>
		</div>
		<div class="row container images">
			<?php 
				foreach($book as $book_list)
				{
					?>
					<div class="col-md-3 img_box">
						<div class="image">
						<img src="<?php echo base_url(); ?>image/<?php echo $book_list->image ?>" class="thumbnail">
						</div>
						<div class="row code">
							<em>Code No:<?php echo " ".$book_list->code_number?></em>
							<a href="<?php echo base_url();?>book/customer_detail/<?php echo $book_list->id; ?>"><button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button></a>
						</div>
					</div>
					<?php
				}
			?>
		</div>
	</body>
</html>