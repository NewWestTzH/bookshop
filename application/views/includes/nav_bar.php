 <style type="text/css">
      .container-fluid
      {
        background-color:#00cca3;
        height: 60px;
      }
      .home
      {
        color: white;
        font-size: 25;
      }
      .down
      {
        margin: 8px 0px 0px 0px;
      }
      .list
      {
        font-size: 25;
      }
      .logout
      {
        font-size: 25;
        color: white;
      }
     </style>
     <?php 
      if(isset($_SESSION['id']))
      { ?>
      <!-- Static navbar -->
      <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url();?>book"><em class="home">Book Store</em></a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo base_url();?>book/create_book"><em class="home">Creat New</em></a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><em class="home">Categories</em><b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url();?>book/admin_browse_category">Category List</a></li>
                  <li><a href="<?php echo base_url();?>author">Author</a></li>
                  <li><a href="<?php echo base_url();?>genre">Genre</a></li>
                  <li><a href="<?php echo base_url();?>publisher">Publishher</a></li>
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo base_url(); ?>login/logout"><em class="logout">Logout</em></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>
      <?php }
      else 
      {?>
        <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand down" href="<?php echo base_url(); ?>book"><em class="home">Book Store</em></a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="dropdown"> 
                <a href="#" class="dropdown-toggle down" data-toggle="dropdown"><em class="list">Book</em><b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url(); ?>book/customer_all_book">All</a></li>
                  <li><a href="<?php echo base_url(); ?>book/customer_search_by_author">Author</a></li>
                  <li><a href="<?php echo base_url(); ?>book/customer_search_by_genre">Genre</a></li>
                  <li><a href="<?php echo base_url(); ?>book/customer_search_by_publisher">Publishher</a></li>
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo base_url(); ?>login"><em class="logout">Login</em></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>
      <?php } ?>
   
