<title>Genre</title>
		<style type="text/css">
			.save_genre
			{
				margin: -20px 0px 0px 200px;
			}
			.jumbotron
			{
				width: 1100px;
				margin-left: 90px;
				background-color: #e6f2ff;
			}
			.footer
			{
				margin: 0px 0px 0px 80px;
			}
			body
			{
			background-color: #e6e6e6;
			}
			tr
			{
				background-color:#00cca3;
			}
			th
			{
				color:white;
			}
			em
		{
			font-size: 20;
		}
		</style>
	<body>
		<div class="jumbotron">	<!-- jumbotron -->
			<div class="save_genre">	<!-- save_genre -->
				<?php echo validation_errors(); ?>
				<?php echo form_open('genre/save_genre');?>
		     		<div class="col-md-2"><label>Genre</label></div>
		     		<div class="col-md-3"><input class="form-control" type="text" name="genre_name" required="true"/></div>
		     		<div class="col-md-3"><input class="btn btn-primary" type="submit" value="Save"/></div>
		     	</form>
		     </div>		<!-- save_genre -->
	     </div>		<!-- jumbotron -->
	     <div class="container">	<!-- container -->
			<table class="display dt-responsive table-striped table-bordered table">
				<thead>
					<tr>
						<th>No.</th>
						<th>Genre</th>
						<th>Update</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$index=1;
						foreach ($query as $genre_list) 
						{
							?>
							<tr>
								<td><?php echo $index; ?></td>
								<td><?php echo $genre_list->genre_name; ?></td>
								<td><button class="btn btn-primary" onClick="showDialog('<?php echo $genre_list->id; ?>','<?php echo $genre_list->genre_name; ?>')"><span class="glyphicon glyphicon-pencil"></span> Update</button></td>
							</tr>
							<?php 
							$index++;
						}
					?>
				</tbody>
			</table>
		</div>		<!-- container -->
		<div class="row">
					<div class="col-md-4 footer"><a href="<?php echo base_url(); ?>book"><em><span class=" glyphicon glyphicon-chevron-left"></span><span class=" glyphicon glyphicon-chevron-left"></span>Back</em></a></div>
		</div>
		<div id="myModal" class="modal fade" role="dialog"> <!-- Modal -->
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
			  		<div class="modal-header">
				    	<button type="button" class="close" data-dismiss="modal">&times;</button>
				    	<h4 class="modal-title">Edit Genre</h4>
			    	</div>
				    <div class="modal-body">
				    	<!-- modal-body -->
				    	<!-- modal-body -->
				    	<!-- modal-body -->
				    </div>
				  	<div class="modal-footer">
				    	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				  	</div>
				</div>

			</div>
		</div> <!-- Modal -->
	    <script type="text/javascript">
			function showDialog(id,name)
			{
				jQuery.ajax(
					{
						url:  "<?php echo base_url();?>genre/modify_genre",
						data:{'genre_id':id ,'genre_name':name},
						type: "POST",
						success:function(data)
							{
								$(".modal-body").html(data);
								//alert(author_id);
								$('#myModal').modal('show');
							},
						error:function (error){alert('error');}
					}
							);
			}
			
		</script>
	<!-- For Datatable -->
	<!--<script type="text/javascript" language="javascript" src="../includes/js/datatable/jquery.dataTables.js"></script>
	<script src="../includes/js/datatable/dataTables.responsive.js"></script>  
	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
			$('table.display').dataTable();
		} );
	</script>-->

	</body>
</html>
