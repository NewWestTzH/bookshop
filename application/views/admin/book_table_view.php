
	<style type="text/css">
	tr
		{
			background-color: #80ffe5;
		}
	</style>
	<table class="display dt-responsive table-striped table-bordered table">
		<thead>
			<tr>
				<th>No.</th>
				<th>Code Number</th>
				<th>Name</th>
				<th>Insert Date</th>
				<th>Author</th>
				<th>Genre</th>
				<th>Publisher</th>
				<th>Price</th>
				<th>Update</th>
			</tr>
		</thead>
		<tbody>
		<?php
				$index=1;
				foreach($book as $book_list)
					{
				?>
						<tr>
							<td><?php echo $index; ?></td>
							<td><a href="<?php echo base_url();?>book/book_detail/<?php echo $book_list->book_id; ?>"><?php echo $book_list->code_number; ?></a></td>
							<td><?php echo $book_list->name; ?></td>
							<td><?php echo $book_list->timestamp; ?></td>
							<td><?php echo $book_list->author_name; ?></td>
							<td><?php echo $book_list->genre_name; ?></td>
							<td><?php echo $book_list->publisher_name; ?></td>
							<td><?php echo $book_list->price; ?></td>
							<td><a href="<?php echo base_url();?>book/update_book/<?php echo $book_list->book_id; ?>">
									<button class="btn btn-primary">
										<span class="glyphicon glyphicon-pencil"></span> 
									Update</button>
								</a>
							</td>
						</tr>
					  <?php
					  $index++;
					}?>
			</tbody>
	</table>
	</div>
	<!-- For Datatable -->
		<script type="text/javascript" language="javascript" src="../includes/js/datatable/jquery.dataTables.js"></script>
		<script src="../includes/js/datatable/dataTables.responsive.js"></script>  
		<script type="text/javascript" language="javascript" class="init">
			$(document).ready(function() {
				$('table.display').dataTable();
			} );
		</script>