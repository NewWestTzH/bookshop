<title>Update Book</title>
		<style type="text/css">
		.row
		{
			padding: 5px 0px 10px 10px;
		}
		body
		{
			background-color: #d9d9d9;
		}
		.form-group
		{
			background-color: #00e6b8;
		}
		.btn
		{
			float: right;
		}
		em
		{
			font-size: 20;
		}
		.body
		{
			margin-left: 200px;
		}
		.photo
		{
			margin-left: 90px;
		}
		#image-label
		{
			height: 350px;
			width: 250px;
		}
	</style>
	</head>
	<body>
		<div class="col-md-5 body">
			<?php echo validation_errors(); ?>
			<?php $attribute=array('method'=>'post','enctype'=>'multipart/form-data')?>
			<?php echo form_open_multipart('book/update_db',$attribute);?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-5">
							<label class="control-label">Code Number</label>
						</div>
						<div class="col-md-5">
							<input type="text" name="book_id" hidden="trued" value="<?php echo $book->book_id; ?>" required="true">
							<input class="form-control" type="text" name="code_number" value="<?php echo $book->code_number ?>" required="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<label class="control-label">Name</label>
						</div>
						<div class="col-md-5">
							<input class="form-control" type="text" name="book_name" value="<?php echo $book->name ?>" required="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<label class="control-label">Author</label>
						</div>
						<div class="col-md-5">
							<select name="author">
							<?php
								foreach($author as $author_list) 
								{
							?>
									<option value="<?php echo $author_list->id; ?>" 
									<?php 
									if($author_list->id==$book->author_id)
										{
											echo "selected";
										}
									?>>
									<?php echo $author_list->author_name; ?>
									</option>
									<?php
								}
							?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<label class="control-label">Genre</label>
						</div>
						<div class="col-md-5">
							<select name="genre">
								<?php
								foreach($genre as $genre_list) 
								{
							?>
									<option value="<?php echo $genre_list->id; ?>" 
									<?php 
									if($genre_list->id==$book->genre_id)
										{
											echo "selected";
										}
									?>>
									<?php echo $genre_list->genre_name; ?>
									</option>
									<?php
								}
							?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<label class="control-label">Publisher</label>
						</div>
						<div class="col-md-5">
							<select name="publisher">
								<?php
								foreach($publisher as $publisher_list) 
								{
							?>
									<option value="<?php echo $publisher_list->id; ?>" 
									<?php 
									if($publisher_list->id==$book->publishing_house_id)
										{
											echo "selected";
										}
									?>>
									<?php echo $publisher_list->publisher_name; ?>
									</option>
									<?php
								}
							?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<label class="control-label">Price</label>
						</div>
						<div class="col-md-5">
							<input class="form-control" type="text" name="price" value="<?php echo $book->price ?>" required="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<label class="control-label">Edition</label>
						</div>
						<div class="col-md-5">
							<input class="form-control" type="text" name="edition" value="<?php echo $book->edition ?>" required="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<label class="ccontrol-label">Book Image</label>
						</div>
						<div class="col-md-5">
							<input id="image-upload" type="file" name="pic" accept="image/*"/><input type="text" name="pic_name" value="<?php echo $book->image; ?>" hidden="trued">
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<label class="control-label">Upload PDF</label>
						</div>
						<div class="col-md-5">
							<input type="file" name="pdf"/><input type="text" name="pdf_name" value="<?php echo $book->save_pdf; ?>" hidden="trued">
						</div>
					</div>
				</div>
					<div class="row">
					<div class="col-md-4"><a href="<?php echo base_url(); ?>book"><em><span class=" glyphicon glyphicon-chevron-left"></span><span class=" glyphicon glyphicon-chevron-left"></span>Back</em></a></div>
					<div class="col-md-4 btn"><input type="submit" name="update_book" value="Save" class="btn btn-primary"></div>
				</div>
			</form>
		</div>
		<div class="col-md-4 photo">
				<div class="image" id="image-label">
					<img src="<?php echo base_url(); ?>image/<?php echo $book->image;?>" class="thumbnail">
					<div class="code">
					<em>Code No:<?php echo " ".$book->code_number?></em>
					</div>
				</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery.uploadPreview.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
		  $.uploadPreview({
		    input_field: "#image-upload",
		    preview_box: "#image-label",
		    label_field: "#image-label"
		  });
		});
		</script>
	</body>
</html>