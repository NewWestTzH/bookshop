<title>Category</title>
		<style type="text/css">
		.box
		{
			margin: 0px 0px 0px 30px;
		}
		.category
		{
			padding: 10px 0px 0px 20px;
		}
		body
			{
			background-color: #d9d9d9;
			}
		tr
		{
			background-color: #80ffe5;
		}
		</style>
	<body>
		<div class="row box">
			<div class="menu col-md-3">
				<a href="#genre" data-toggle="collapse">
					<li class="list-group-item list-group-item-info"><h4 class="category">Genre</h4></li>
				</a>
				<div id="genre" class="collapse">
					<?php 
						foreach($genre as $genre_list) 
							{
								?>
								<li class="list-group-item list-group-item-info"><p OnClick="get_data('genre','<?php echo $genre_list->id; ?>')" style="cursor: pointer;"><?php echo $genre_list->genre_name ?></p></li>
								<?php
							}
					?> 
				</div>
				<a href="#author" data-toggle="collapse">
					<li class="list-group-item list-group-item-danger"><h4 class="category">Author</h4></li>
				</a>
				<div id="author" class="collapse">
					<?php 
						foreach($author as $author_list) 
							{
								?><li class="list-group-item list-group-item-danger"><p OnClick="get_data('author','<?php echo $author_list->id ;?>')" style="cursor: pointer;"><?php echo $author_list->author_name ?></p></li>
									<?php
							}
							?> 
				</div>
				<a href="#publisher" data-toggle="collapse">
					<li class="list-group-item list-group-item-success"><h4 class="category">Publisher</h4></li>
				</a>
				<div id="publisher" class="collapse">
					<?php 
						foreach($publisher as $publisher_list) 
							{
								?><li class="list-group-item list-group-item-success"><p OnClick="get_data('publisher','<?php echo $publisher_list->id ;?>')" style="cursor: pointer;"><?php echo $publisher_list->publisher_name ?></p></li>
								<?php
							}
						?> 
				</div>
			</div>
			<div class="col-md-9 tableshow">
				<table class="display dt-responsive table-striped table-bordered table">
			<thead>
				<tr>
					<th>No.</th>
					<th>Code Number</th>
					<th>Name</th>
					<th>Insert Date</th>
					<th>Author</th>
					<th>Genre</th>
					<th>Publisher</th>
					<th>Price</th>
					<th>Update</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$index=1;
					foreach($book as $book_list)
					{
				?>
						<tr>
							<td><?php echo $index; ?></td>
							<td><a href="<?php echo base_url();?>book/book_detail/<?php echo $book_list->book_id;?>"><?php echo $book_list->code_number; ?></a></td>
							<td><?php echo $book_list->name; ?></td>
							<td><?php echo $book_list->timestamp; ?></td>
							<td><?php echo $book_list->author_name; ?></td>
							<td><?php echo $book_list->genre_name; ?></td>
							<td><?php echo $book_list->publisher_name; ?></td>
							<td><?php echo $book_list->price; ?></td>
							<td><a href="<?php echo base_url();?>book/update_book/<?php echo $book_list->book_id; ?>">
									<button class="btn btn-primary">
										<span class="glyphicon glyphicon-pencil"></span> 
									Update</button>
								</a>
							</td>
						  </tr>
					  <?php
					  $index++;
					}?>
			</tbody>
		</table>
			</div>
		</div>
	<script type="text/javascript">
			function get_data(type,id)
			{ 
				jQuery.ajax({
						url:  "<?php echo base_url();?>book/browse_book_by_category",
						data:{'id':id ,'type':type},
						type: "POST",
						success:function(data)
							{
								$(".tableshow").html(data);
							},
						error:function (error){alert(error);}
					}
							);
			}
		</script>
	</body>
</html>