<title>Author</title>
		<style type="text/css">
			.save_author
			{
				margin: -20px 0px 0px 200px;
			}
			.jumbotron
			{
				width: 1100px;
				margin-left: 90px;
				background-color: #e6f2ff;
			}
			.footer
			{
				margin: 0px 0px 20px 80px;
			}
			body
			{
				background-color: #e6e6e6;
			}
			tr
			{
				background-color:#00cca3;
			}
			th
			{
				color:white;
			}
			em
			{
				font-size: 20;
			}
		</style>
	<body>
		<div class="jumbotron"> <!-- jumbotron -->
			<div class="save_author">  <!-- save_author -->
				<?php echo validation_errors(); ?>
				<?php echo form_open('author/save_author');?>
		     		<div class="col-md-2"><label>Author Name</label></div>
		     		<div class="col-md-3"><input class="form-control" type="text" name="author_name" required="true"/></div>
		     		<div class="col-md-3"><input class="btn btn-primary" type="submit" value="Save"/></div>
		     	</form>
		     </div>   <!-- //save_author -->
	     </div> <!-- //jumbotron -->
	     <div class="container">	<!-- container -->
			<table class="display dt-responsive table-striped table-bordered table">
				<thead>
					<tr>
						<th>No.</th>
						<th>Author</th>
						<th>Update</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$index=1;
						foreach ($query as $author_list) 
						{
							?>
							<tr>
								<td><?php echo $index; ?></td>
								<td><?php echo $author_list->author_name; ?></td>
								<td><button class="btn btn-primary" onClick="showDialog('<?php echo $author_list->id; ?>','<?php echo $author_list->author_name; ?>')"><span class="glyphicon glyphicon-pencil"></span> Update</button></td>
							</tr>
							<?php 
							$index++;
						}
					?>
				</tbody>
			</table>
		</div>	<!-- //container -->
		<div class="row">	<!-- row -->
			<div class="col-md-4 footer">	<!-- footer -->
				<a href="<?php echo base_url(); ?>book">
					<em><span class=" glyphicon glyphicon-chevron-left"></span>
						<span class=" glyphicon glyphicon-chevron-left"></span>Back
					</em>
				</a>
			</div>	<!-- //footer -->
		</div>	<!-- //row -->
		<div id="myModal" class="modal fade" role="dialog"> <!-- Modal -->
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
			  		<div class="modal-header">
				    	<button type="button" class="close" data-dismiss="modal">&times;</button>
				    	<h4 class="modal-title">Edit Author</h4>
			    	</div>
				    <div class="modal-body">
				    	<!-- modal-body -->
				    	<!-- modal-body -->
				    	<!-- modal-body -->
				    </div>
				  	<div class="modal-footer">
				    	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				  	</div>
				</div>

			</div>
		</div> <!-- //Modal -->
	    <script type="text/javascript">
			function showDialog(id,name)
			{//alert('id'+author_id);
				jQuery.ajax(
					{
						url:  "<?php echo base_url();?>author/modify_author",
						data:{'author_id':id ,'author_name':name},
						type: "POST",
						success:function(data)
							{
								$(".modal-body").html(data);
								//alert(author_id);
								$('#myModal').modal('show');
							},
						error:function (error){alert('error');}
					}
							);
			}
		</script>
	</body>
</html>
