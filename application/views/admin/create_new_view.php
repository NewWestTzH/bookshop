	   <title>Create Book</title>
		<style type="text/css">
		.row
		{
			padding: 5px 0px 10px 10px;
		}
		body
		{
			background-color: #d9d9d9;
		}
		.body
		{
			margin: 0px 0px 0px 250px;
			width: 500px;
		}
		.form-group
		{
			background-color: #00e6b8;
		}
		.btn
		{
			float: right;
		}
		.text
		{
			font-size: 16;
		}
		em
		{
			font-size: 15;
		}
		#image-label
		{
			height: 350px;
			width: 250px;
		}
	</style>
	<body>
		<div class="row">
			<div class="body col-md-8 ">
				<?php echo validation_errors(); ?>
				<?php $attribute=array('method'=>'post','enctype'=>'multipart/form-data')?>
				<?php echo form_open_multipart('book/insert_book',$attribute);?>
					<div class="form-group">
						<div class="row">
							<div class="col-md-5">
								<label class="control-label"><em class="text">Code Number</em></label>
							</div>
							<div class="col-md-5">
								<input class="form-control" type="text" name="code_number" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="control-label"><em class="text">Name</em></label>
							</div>
							<div class="col-md-5">
								<input class="form-control" type="text" name="book_name" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="control-label"><em class="text">Author</em></label>
							</div>
							<div class="col-md-5">
								<select name="author">
								<?php
									foreach($author as $author_list) 
									{
								?>
										<option value="<?php echo $author_list->id; ?>">
										<?php echo $author_list->author_name; ?>
										</option>
										<?php
									}
								?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="control-label"><em class="text">Genre</em></label>
							</div>
							<div class="col-md-5">
								<select name="genre">
									<?php
									foreach($genre as $genre_list) 
									{
								?>
										<option value="<?php echo $genre_list->id; ?>">
										<?php echo $genre_list->genre_name; ?>
										</option>
										<?php
									}
								?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="control-label"><em class="text">Publisher</em></label>
							</div>
							<div class="col-md-5">
								<select name="publisher">
									<?php
									foreach($publisher as $publisher_list) 
									{
								?>
										<option value="<?php echo $publisher_list->id ; ?>">
										<?php echo $publisher_list->publisher_name ; ?>
										</option>
										<?php
									}
								?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="control-label"><em class="text">Price</em></label>
							</div>
							<div class="col-md-5">
								<input class="form-control" type="text" name="price" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="control-label"><em class="text">Edition</em></label>
							</div>
							<div class="col-md-5">
								<input class="form-control" type="text" name="edition" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="ccontrol-label"><em class="text">Book Image</em></label>
							</div>
							<div class="col-md-5">
								<input type="file" accept="image/*" name="pic" id="image-upload"/>
								<!-- <label for="image-upload" id="image-label">Choose File</label> -->
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<label class="control-label" accept=".pdf"><em class="text">Upload PDF</em></label>
							</div>
							<div class="col-md-5">
								<input type="file" name="pdf"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><a href="<?php echo base_url(); ?>book"><em><span class=" glyphicon glyphicon-chevron-left"></span><span class=" glyphicon glyphicon-chevron-left"></span>Back</em></a></div>
						<div class="col-md-4 btn"><input type="submit" name="create_book" value="Save" class="btn btn-primary"></div>
					</div>
				</form>
			</div>
			<div class="col-md-3"><label for="image-upload" id="image-label"></label></div>
		</div>
		<script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery.uploadPreview.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
		  $.uploadPreview({
		    input_field: "#image-upload",
		    preview_box: "#image-label",
		    label_field: "#image-label"
		  });
		});
		</script>
	</body>
</html>