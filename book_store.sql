-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 03, 2016 at 04:33 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `book_store`
--
CREATE DATABASE IF NOT EXISTS `book_store` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `book_store`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'mtk@gmail.com', 'mtk');

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE IF NOT EXISTS `author` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `author_name`) VALUES
(1, 'Ann Coulter'),
(2, 'Jelly'),
(3, 'Jerry'),
(4, 'KFC'),
(5, 'N.D Arora'),
(6, 'Kelvin 2');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `code_number` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `publishing_date` date NOT NULL,
  `description` varchar(225) NOT NULL,
  `image` varchar(100) NOT NULL,
  `save_pdf` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` int(30) NOT NULL DEFAULT '0',
  `deleted_by` int(30) NOT NULL,
  `author_id` int(30) NOT NULL,
  `genre_id` int(30) NOT NULL,
  `publishing_house_id` int(30) NOT NULL,
  `inserted_by` int(30) NOT NULL,
  `edition` int(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `code_number`, `name`, `price`, `publishing_date`, `description`, `image`, `save_pdf`, `timestamp`, `deleted`, `deleted_by`, `author_id`, `genre_id`, `publishing_house_id`, `inserted_by`, `edition`) VALUES
(1, '1', 'Peoples Magazine', 3000, '0000-00-00', '', 'people_magazine3.jpg', 'PHP3.pdf', '2016-08-05 08:54:31', 0, 0, 2, 2, 1, 0, 1),
(2, '6', 'Einstein', 15000, '0000-00-00', '', 'einstein.jpg', 'PHP.pdf', '2016-08-10 03:39:12', 1, 0, 1, 1, 1, 0, 2),
(3, '2', 'Java', 15000, '0000-00-00', '', 'java.jpg', 'PHP1.pdf', '2016-08-11 07:34:04', 0, 0, 3, 1, 2, 0, 4),
(4, '8', 'Pure PHP', 3000, '0000-00-00', '', 'index.jpg', 'PHP2.pdf', '2016-08-09 09:44:19', 0, 0, 1, 1, 1, 0, 2),
(5, '3', 'Einstein', 15000, '0000-00-00', '', 'einstein.jpg', 'PHP.pdf', '2016-08-10 03:59:09', 1, 0, 1, 2, 1, 0, 2),
(6, '4', 'Einstein', 15000, '0000-00-00', '', 'einstein2.jpg', 'PHP.pdf', '2016-08-10 08:26:08', 0, 0, 1, 2, 2, 0, 2),
(7, '5', 'ASPNet', 15000, '0000-00-00', '', 'asp_net.jpg', 'sql.pdf', '2016-08-10 08:31:53', 0, 0, 4, 1, 3, 0, 2),
(8, '7', 'Political Sucide', 15000, '0000-00-00', '', 'political_sucide.jpg', 'OOP.pdf', '2016-08-10 08:41:36', 0, 0, 3, 3, 1, 0, 1),
(9, '9', 'C#', 15000, '0000-00-00', '', 'c.jpg', 'PHP4.pdf', '2016-08-10 11:04:04', 0, 0, 1, 1, 1, 0, 1),
(10, '10', 'VB.Net', 15000, '0000-00-00', '', 'vbnet1.jpg', 'PHP5.pdf', '2016-08-10 11:05:05', 0, 0, 1, 1, 2, 0, 2),
(11, '11', 'Political Science', 3000, '0000-00-00', '', 'political_science.jpg', 'sql1.pdf', '2016-08-11 07:20:57', 0, 0, 3, 3, 2, 0, 4),
(12, '12', 'Peoples Magazine', 3000, '0000-00-00', '', 'time_magazine.jpg', 'PHP6.pdf', '2016-08-11 07:22:46', 0, 0, 4, 4, 3, 0, 1),
(13, '17', 'JavaScript and Jquery', 15000, '0000-00-00', '', 'script.jpg', 'sql2.pdf', '2016-08-18 04:42:18', 0, 0, 3, 1, 3, 0, 4),
(14, '22', 'Flower', 3000, '0000-00-00', '', 'american_lion1.jpg', 'PHP7.pdf', '2016-08-18 07:18:09', 1, 0, 4, 3, 3, 0, 3),
(15, '33', 'DOG', 15000, '0000-00-00', '', 'einstein3.jpg', 'PHP8.pdf', '2016-08-18 07:22:14', 0, 0, 1, 1, 1, 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `genre_name`) VALUES
(1, 'IT'),
(2, 'Journal'),
(3, 'Political'),
(4, 'Magazine');

-- --------------------------------------------------------

--
-- Table structure for table `publisher`
--

CREATE TABLE IF NOT EXISTS `publisher` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `publisher_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `publisher`
--

INSERT INTO `publisher` (`id`, `publisher_name`) VALUES
(1, 'News Feed'),
(2, 'Banana'),
(3, 'Apple');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
